FROM openjdk:11.0.7-slim-buster
MAINTAINER Nanoo (arnaudlaval33@gmail.com)
ADD target/commentary-management.jar commentary-management.jar
ENTRYPOINT ["java","-jar","commentary-management.jar"]
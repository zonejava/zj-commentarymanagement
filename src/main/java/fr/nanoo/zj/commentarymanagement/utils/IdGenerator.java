package fr.nanoo.zj.commentarymanagement.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/**
 * @author nanoo - created : 12/06/2020 - 09:23
 */
@Slf4j
@UtilityClass
public class IdGenerator {

    /**
     * This method generate randomly an id like keycloak auth server does.
     * @return id
     */
    public String generateExternalId() {

        // modele
        int[] modelStructure = {8, 4, 4, 4, 12};

        // chose a Character random from this String
        String alphaNumericString = "abcdefghijklmnopqrstuvxyz"
                                  + "0123456789";

        // create StringBuffer size of alphaNumericString
        StringBuilder sb = new StringBuilder();

        for (int value : modelStructure) {
            for (int j = 0; j < value; j++) {

                // generate a random number between
                // 0 to alphaNumericString variable length
                int index = (int) (alphaNumericString.length() * Math.random());

                // add Character one by one in end of sb
                sb.append(alphaNumericString.charAt(index));
            }
            sb.append("-");
        }

        // remove last catactère "-"
        sb.setLength(sb.length()-1);

        return sb.toString();

    }

}

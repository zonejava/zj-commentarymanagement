package fr.nanoo.zj.commentarymanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ZjCommentaryManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZjCommentaryManagementApplication.class, args);
    }

}

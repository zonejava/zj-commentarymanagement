package fr.nanoo.zj.commentarymanagement.security;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

/**
 * @author nanoo - created : 08/07/2020 - 09:53
 */
@Slf4j
@UtilityClass
public class SecurityUtils {

    /**
     * check if user is authorized (owner or admin)
     * If not, throw AccessDeniedException : code 403 forbidden
     * @param ownerId id of owner
     */
    public void checkIfAuthorized(String ownerId){
        if (!checkIfAdmin() && !checkIfOwner(ownerId))
            throw new AccessDeniedException("User is not the owner or an admin - Access denied");
    }

    /**
     * Compare owner id and logged user Id
     * @param ownerId id of owner
     * @return true if user is the owner
     */
    private boolean checkIfOwner(String ownerId){
        return ownerId.equals(SecurityContextHolder.getContext().getAuthentication().getCredentials().toString());
    }

    /**
     * Look if logged user has role admin
     * @return true if user is an admin
     */
    private boolean checkIfAdmin(){
        Collection<? extends GrantedAuthority> loggedUserRoles = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority grantedAuthority : loggedUserRoles){
            String[] roles = grantedAuthority.getAuthority()
                    .substring(1,(grantedAuthority.getAuthority().length() - 1))
                    .replace(" ","")
                    .split(",");
            for (String role : roles){
                log.debug(role);
                if (role.equalsIgnoreCase("user")) {
                    return true;
                }
            }
        }
        return false;
    }

}

package fr.nanoo.zj.commentarymanagement.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @author nanoo - created : 28/05/2020 - 17:01
 */
@Slf4j
public class JwtTokenAuthorizationFilter extends OncePerRequestFilter {

    /* Config */
    public static final String HEADER = "Authorization";
    public static final String PREFIX = "Bearer ";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        // 1. Check for token in request
        String fullToken = request.getHeader(HEADER);

        // 2. If there is no token the user won't be authenticated.
        if(fullToken == null) {
            chain.doFilter(request, response);
            return;
        }

        try {

            // pre 4. handle token begin with "Bearer" (Postman test automatically add it to token)
            String token;
            if (fullToken.contains("Bearer"))
                token = fullToken.replace(PREFIX,"");
            else
                token = fullToken;

            // 4. Validate the token
            DecodedJWT jwt = JWT.decode(token);
            if (jwt.getExpiresAt().before(new Date()))
                throw new JWTVerificationException("Token expired");

            String username = jwt.getClaim("preferred_username").asString();
            if(username != null) {

                //Try to get list<GrantedAuthority from claim object in token
                Map<String, Object> accessClaims = jwt.getClaim("realm_access").asMap(); // get 'realm_access' as map
                List<Object> rolesClaims = Collections.singletonList(accessClaims.get("roles")); // get 'roles' part content in a list of Object

                List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
                for (Object role: rolesClaims){
                    grantedAuthorities.add(new SimpleGrantedAuthority(role.toString()));
                }

                // Get the id to use it as security context test
                String externalId = jwt.getClaim("sub").asString();

                // 5. Create auth object
                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                        username, externalId, grantedAuthorities);

                // 6. Authenticate the user
                SecurityContextHolder.getContext().setAuthentication(auth);
            }

        } catch (Exception e) {
            // In case of failure. Make sure user won't be authenticated
            SecurityContextHolder.clearContext();
        }

        chain.doFilter(request, response);
    }

}

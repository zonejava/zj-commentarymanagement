package fr.nanoo.zj.commentarymanagement.application.errors;

/**
 * @author nanoo - created : 03/06/2020 - 10:13
 */
public class FunctionalException extends RuntimeException {

    public FunctionalException(String message) {
        super(message);
    }

}

package fr.nanoo.zj.commentarymanagement.application.controller;

import fr.nanoo.zj.commentarymanagement.application.errors.FunctionalException;
import fr.nanoo.zj.commentarymanagement.model.dtos.CommentaryDto;
import fr.nanoo.zj.commentarymanagement.service.contract.CommentaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

/**
 * @author nanoo - created : 30/06/2020 - 11:39
 */
@RestController
@Slf4j
@PreAuthorize("isAuthenticated()")
public class CommentaryController {

    private final CommentaryService commentaryService;

    @Autowired
    public CommentaryController(CommentaryService commentaryService) {
        this.commentaryService = commentaryService;
    }

    @PutMapping("/commentary/save")
    public List<CommentaryDto> saveCommentary(@RequestBody CommentaryDto commentaryDto){
        log.debug("save commentary method entry");
        try {
            CommentaryDto saved = commentaryService.saveCommentary(commentaryDto);
            return commentaryService.getCommentaries(saved.getCommentedId());
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during commentary insertion in database", e
            );
        }
    }

    @PreAuthorize("hasRole('admin') or #commentaryDto.writer.externalId == authentication.credentials")
    @PutMapping("/commentary/update")
    public List<CommentaryDto> updateCommentary(@RequestBody CommentaryDto commentaryDto){
        log.debug("save commentary method entry");
        try {
            CommentaryDto updated = commentaryService.updateCommentary(commentaryDto);
            return commentaryService.getCommentaries(updated.getCommentedId());
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during commentary update in database", e
            );
        }
    }

    @DeleteMapping("/commentary/delete/{id}")
    public void deleteCommentary(@PathVariable("id") String id){
        log.debug("delete commentary method entry");
        try {
            commentaryService.deleteCommentary(id);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during commentary deletion from database", e
            );
        }
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/commentaries/{id}")
    public List<CommentaryDto> getCommentaries(@PathVariable("id") String id){
        log.debug("get commentaries method entry");
        try {
            return commentaryService.getCommentaries(id);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error, commentaries not found in database", e
            );
        }
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/commentaries/count")
    public Map<String,Integer> getCommentariesCount(@RequestParam Map<String, Integer> idCountMap){
        log.debug("get commentaries count method entry");
        try {
            return commentaryService.getCommentariesCount(idCountMap);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during getting commentary count process", e
            );
        }
    }

}

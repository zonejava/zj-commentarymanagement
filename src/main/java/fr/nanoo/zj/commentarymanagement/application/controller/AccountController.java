package fr.nanoo.zj.commentarymanagement.application.controller;

import fr.nanoo.zj.commentarymanagement.model.dtos.AccountDto;
import fr.nanoo.zj.commentarymanagement.service.contract.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author nanoo - created : 10/06/2020 - 19:48
 */
@RestController
@Slf4j
@PreAuthorize("isAuthenticated()")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PreAuthorize("permitAll()")
    @PutMapping("/account/save")
    public AccountDto saveUser(@RequestBody AccountDto accountDto){
        log.debug("save account method entry");
        try {
            return accountService.saveUser(accountDto);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during user insertion in tutorial service database", e
            );
        }
    }

    @PreAuthorize("hasRole('admin') or #id == authentication.credentials")
    @DeleteMapping("/account/remove/{id}")
    public void removeAccount(@PathVariable("id") String id){
        log.debug("delete account by external id entry");
        try {
            accountService.deleteUserAccount(id);
        }catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during user deletion in tutorial service database", e
            );
        }
    }

}

package fr.nanoo.zj.commentarymanagement.db;

import fr.nanoo.zj.commentarymanagement.model.entities.Commentary;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author nanoo - created : 30/06/2020 - 10:57
 */
@Repository
public interface CommentaryRepository extends JpaRepository<Commentary, Long> {

    List<Commentary> findAllByCommentedId (String commentedId, Sort sort);

    int countCommentariesByCommentedId (String id);

    Optional<Commentary> findByExternalId(String externalId);
}

package fr.nanoo.zj.commentarymanagement.db;

import fr.nanoo.zj.commentarymanagement.model.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author nanoo - created : 30/06/2020 - 10:58
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Modifying
    @Query(value="DELETE FROM Account user WHERE user.externalId = :externalId")
    void deleteByExternalId(String externalId);

    Optional<Account> findByExternalId(String externalId);

}

package fr.nanoo.zj.commentarymanagement.service.contract;

import fr.nanoo.zj.commentarymanagement.model.dtos.CommentaryDto;

import java.util.List;
import java.util.Map;

/**
 * @author nanoo - created : 30/06/2020 - 11:32
 */
public interface CommentaryService {

    /**
     * This method get all commentaries for one content.
     * @param commentedId id of content we want commentaries
     * @return a list of commentary if exist
     */
    List<CommentaryDto> getCommentaries (String commentedId);

    /**
     * This method save a commentary in DB.
     * @param commentaryDto commentary to save
     * @return the commentary saved if success
     */
    CommentaryDto saveCommentary (CommentaryDto commentaryDto);

    /**
     * this method update a commentary
     * @param commentaryDto new version of commentary
     * @return commentary updated
     */
    CommentaryDto updateCommentary(CommentaryDto commentaryDto);

    /**
     * This method remove a commentary from DB.
     * @param id id of commentary to remove
     */
    void deleteCommentary (String id);

    /**
     * This method get the count of commentaries in DB for each id contain in idCountMap
     * @param idCountMap map who contains id as key
     * @return same map with count as value for each id/key
     */
    Map<String, Integer> getCommentariesCount(Map<String, Integer> idCountMap);
}

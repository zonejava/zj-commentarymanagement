package fr.nanoo.zj.commentarymanagement.service.contract;

import fr.nanoo.zj.commentarymanagement.model.dtos.AccountDto;
import fr.nanoo.zj.commentarymanagement.model.entities.Account;

/**
 * @author nanoo - created : 30/06/2020 - 11:04
 */
public interface AccountService {

    /**
     * This method save a user in DataBase.
     * @param user user to save
     * @return user if save is success
     */
    AccountDto saveUser(AccountDto user);

    /**
     * This method remove a user from Database.
     * @param id user id to delete
     */
    void deleteUserAccount(String id);

    /**
     * This method get account information in DB.
     * @param externalId id of user
     * @return user
     */
    Account getUserAccount(String externalId);

}

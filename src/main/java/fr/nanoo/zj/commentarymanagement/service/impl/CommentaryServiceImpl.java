package fr.nanoo.zj.commentarymanagement.service.impl;

import fr.nanoo.zj.commentarymanagement.application.errors.FunctionalException;
import fr.nanoo.zj.commentarymanagement.db.CommentaryRepository;
import fr.nanoo.zj.commentarymanagement.model.dtos.CommentaryDto;
import fr.nanoo.zj.commentarymanagement.model.entities.Account;
import fr.nanoo.zj.commentarymanagement.model.entities.Commentary;
import fr.nanoo.zj.commentarymanagement.model.mappers.CommentaryMapper;
import fr.nanoo.zj.commentarymanagement.security.SecurityUtils;
import fr.nanoo.zj.commentarymanagement.service.contract.AccountService;
import fr.nanoo.zj.commentarymanagement.service.contract.CommentaryService;
import fr.nanoo.zj.commentarymanagement.utils.IdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author nanoo - created : 30/06/2020 - 11:36
 */
@Service
@Transactional
@Slf4j
public class CommentaryServiceImpl implements CommentaryService {

    private final CommentaryRepository commentaryRepository;
    private final CommentaryMapper commentaryMapper;

    private final AccountService accountService;

    @Autowired
    public CommentaryServiceImpl(CommentaryRepository commentaryRepository, CommentaryMapper commentaryMapper, AccountService accountService) {
        this.commentaryRepository = commentaryRepository;
        this.commentaryMapper = commentaryMapper;
        this.accountService = accountService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CommentaryDto> getCommentaries(String commentedId) {
        return commentaryMapper.fromEntityListToDtoList(
                commentaryRepository.findAllByCommentedId(commentedId, Sort.by(Sort.Direction.DESC, "creationCommentaryDate"))
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentaryDto saveCommentary(CommentaryDto commentaryDto) {
        Commentary commentaryToSave = commentaryMapper.fromDtoToEntity(commentaryDto);

        /* First retrieve writer info */
        String writerId = AccountServiceImpl.getUserLoggedExternalId();
        Account writer = accountService.getUserAccount(writerId);
        if (writer == null) throw new FunctionalException("No writer account found in DB");

        /* Second fill some field in tutorial */
        commentaryToSave.setCreationCommentaryDate(LocalDateTime.now());
        commentaryToSave.setExternalId(IdGenerator.generateExternalId());
        commentaryToSave.setWriter(writer);

        return commentaryMapper.fromEntityToDto(commentaryRepository.save(commentaryToSave));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentaryDto updateCommentary(CommentaryDto commentaryDto){
        Commentary oldCommentary =
                commentaryRepository.findByExternalId(commentaryDto.getExternalId()).orElse(null);
        if (oldCommentary == null )
            throw new FunctionalException("Error during commentary update, old version not found in database");

        oldCommentary.setUpdateCommentaryDate(LocalDateTime.now());
        oldCommentary.setContent(commentaryDto.getContent());

        return commentaryMapper.fromEntityToDto(oldCommentary);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteCommentary(String id) {
        try{
            Commentary commentaryToDelete = commentaryRepository.findByExternalId(id).orElse(null);
            if (commentaryToDelete == null)
                throw new FunctionalException("Error : Commentary not found in DB");

            SecurityUtils.checkIfAuthorized(commentaryToDelete.getWriter().getExternalId());

            commentaryRepository.delete(commentaryToDelete);
        }catch (HibernateException e){
            log.error("error during commentary deletion");
            throw new FunctionalException("error during commentary deletion : " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Integer> getCommentariesCount(Map<String, Integer> idCountMap) {
        for (String s : idCountMap.keySet()) {
            idCountMap.replace(s,commentaryRepository.countCommentariesByCommentedId(s));
        }
        return idCountMap;
    }
}

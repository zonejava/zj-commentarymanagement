package fr.nanoo.zj.commentarymanagement.model.mappers;

import fr.nanoo.zj.commentarymanagement.model.dtos.AccountDto;
import fr.nanoo.zj.commentarymanagement.model.entities.Account;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author nanoo - created : 30/06/2020 - 10:47
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapper {

    AccountDto fromEntityToDto(Account account);
    Account fromDtoToEntity(AccountDto accountDto);

}

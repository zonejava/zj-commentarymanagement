package fr.nanoo.zj.commentarymanagement.model.dtos;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 30/06/2020 - 10:39
 */
@Data
public class CommentaryDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String externalId;
    private String commentedId;
    private String content;
    private AccountDto writer;
    private LocalDateTime creationCommentaryDate;
    private LocalDateTime updateCommentaryDate;

}

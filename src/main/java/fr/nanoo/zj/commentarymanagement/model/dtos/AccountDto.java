package fr.nanoo.zj.commentarymanagement.model.dtos;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 10/06/2020 - 12:54
 */
@Data
public class AccountDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String externalId;
    private String firstName;
    private String lastName;
    private String password;
    private String username;
    private String email;
    private String biography;
    private LocalDateTime registrationAccountDate;

}

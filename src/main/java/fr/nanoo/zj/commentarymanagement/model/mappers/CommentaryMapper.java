package fr.nanoo.zj.commentarymanagement.model.mappers;

import fr.nanoo.zj.commentarymanagement.model.dtos.CommentaryDto;
import fr.nanoo.zj.commentarymanagement.model.entities.Commentary;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author nanoo - created : 30/06/2020 - 10:41
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = AccountMapper.class)
public interface CommentaryMapper {

    CommentaryDto fromEntityToDto(Commentary commentary);
    Commentary fromDtoToEntity(CommentaryDto commentaryDto);

    List<Commentary> fromDtoListToEntityList(List<CommentaryDto> commentaryDtos);
    List<CommentaryDto> fromEntityListToDtoList(List<Commentary> commentaries);

}

package fr.nanoo.zj.commentarymanagement.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 29/06/2020 - 16:50
 */
@Data
@Entity
@Table(name = "commentary")
public class Commentary implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_commentary")
    private Long commentaryId;

    @Column(name = "external_id",unique = true, nullable = false)
    private String externalId;

    @Column(name = "commented_id", nullable = false)
    private String commentedId;

    @Column(name = "content", nullable = false, length = 500)
    private String content;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="id_account", nullable=false)
    private Account writer;

    @Column(name = "creation_commentary", nullable = false)
    private LocalDateTime creationCommentaryDate;

    @Column(name = "update_commentary")
    private LocalDateTime updateCommentaryDate;

}

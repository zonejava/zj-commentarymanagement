package fr.nanoo.zj.commentarymanagement.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 29/06/2020 - 16:56
 */
@Data
@Entity
@Table(name = "account")
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_account")
    private Long accountId;

    @Column(name = "external_id",unique = true, nullable = false)
    private String externalId;

    @Column(name = "first_name", length = 50, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 50, nullable = false)
    private String lastName;

    @Column(name = "username", length = 50, nullable = false)
    private String username;

    @Column(name = "registration_account", nullable = false)
    private LocalDateTime registrationAccountDate;

    @Column(name = "update_account")
    private LocalDateTime updateAccountDate;

}

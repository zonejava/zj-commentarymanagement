package fr.nanoo.zj.commentarymanagement.service.impl;

import fr.nanoo.zj.commentarymanagement.db.CommentaryRepository;
import fr.nanoo.zj.commentarymanagement.model.dtos.CommentaryDto;
import fr.nanoo.zj.commentarymanagement.model.entities.Account;
import fr.nanoo.zj.commentarymanagement.model.entities.Commentary;
import fr.nanoo.zj.commentarymanagement.service.contract.CommentaryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class CommentaryServiceImplIT {

    @Autowired
    private CommentaryService commentaryService;
    @Autowired
    private CommentaryRepository commentaryRepository;

    @BeforeEach
    void init(){
        commentaryRepository.deleteAll();

        Account account1 = new Account();
        account1.setExternalId("xxx1");
        account1.setFirstName("jean");
        account1.setLastName("marc");
        account1.setRegistrationAccountDate(LocalDateTime.now());
        account1.setUsername("jeanmi");
        Account account2 = new Account();
        account2.setExternalId("xxx2");
        account2.setFirstName("marcel");
        account2.setLastName("avis");
        account2.setRegistrationAccountDate(LocalDateTime.now());
        account2.setUsername("clavi");

        Commentary commentary1 = new Commentary();
        commentary1.setWriter(account1);
        commentary1.setCreationCommentaryDate(LocalDateTime.now());
        commentary1.setContent("commentaire 1");
        commentary1.setCommentedId("1");
        commentary1.setExternalId("xxx1");
        Commentary commentary2 = new Commentary();
        commentary2.setWriter(account2);
        commentary2.setCreationCommentaryDate(LocalDateTime.now());
        commentary2.setContent("commentaire 2");
        commentary2.setCommentedId("2");
        commentary2.setExternalId("xxx2");

        commentaryRepository.saveAll(Arrays.asList(commentary1,commentary2));
    }

    @Test
    void getCommentaries() {
        List<CommentaryDto> result = commentaryService.getCommentaries("1");
        assertThat(result).hasSize(1);
    }

    @Test
    void getCommentariesCount(){
        Map<String, Integer> param = new HashMap<>();
        param.put("1",0);
        Map<String, Integer> result = commentaryService.getCommentariesCount(param);
        assertThat(result).hasSize(1).containsEntry("1",1);
    }
}
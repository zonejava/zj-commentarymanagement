package fr.nanoo.zj.commentarymanagement.service.impl;

import fr.nanoo.zj.commentarymanagement.application.errors.FunctionalException;
import fr.nanoo.zj.commentarymanagement.db.CommentaryRepository;
import fr.nanoo.zj.commentarymanagement.model.dtos.CommentaryDto;
import fr.nanoo.zj.commentarymanagement.model.mappers.AccountMapperImpl;
import fr.nanoo.zj.commentarymanagement.model.mappers.CommentaryMapper;
import fr.nanoo.zj.commentarymanagement.model.mappers.CommentaryMapperImpl;
import fr.nanoo.zj.commentarymanagement.service.contract.AccountService;
import fr.nanoo.zj.commentarymanagement.service.contract.CommentaryService;
import org.hibernate.HibernateException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringJUnitConfig(value = {CommentaryServiceImpl.class, CommentaryMapperImpl.class, AccountMapperImpl.class})
class CommentaryServiceImplTest {

    @Autowired
    private CommentaryService commentaryService;
    @Autowired
    private CommentaryMapper commentaryMapper;

    @MockBean
    private CommentaryRepository commentaryRepository;
    @MockBean
    private AccountService accountService;

    private CommentaryDto commentaryDto;

    @BeforeEach
    public void init(){
        commentaryDto = new CommentaryDto();
    }

    @Test
    void whenSaveCommentary_GivenNoAccountInAccountRepository_ThenThrowFunctionalException() {
        when(accountService.getUserAccount(any(String.class))).thenReturn(null);

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> commentaryService.saveCommentary(commentaryDto));

        assertThat(exception.getMessage()).isEqualTo("No writer account found in DB");
    }

    @Test
    void whenUpdateCommentary_GivenNoCommentaryInCommentaryRepository_ThenThrowFunctionalException() {
        when(commentaryRepository.findByExternalId(any(String.class))).thenReturn(Optional.empty());

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> commentaryService.updateCommentary(commentaryDto));

        assertThat(exception.getMessage()).isEqualTo("Error during commentary update, old version not found in database");
    }

    @Test
    void whenDeleteCommentary_GivenNoCommentaryInCommentaryRepository_ThenThrowFunctionalException() {
        when(commentaryRepository.findByExternalId(any(String.class))).thenReturn(Optional.empty());

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> commentaryService.deleteCommentary("test"));

        assertThat(exception.getMessage()).isEqualTo("Error : Commentary not found in DB");
    }

    @Test
    void whenDeleteCommentary_GivenErrorThrownInCommentaryRepository_ThenThrowFunctionalException() {
        when(commentaryRepository.findByExternalId(any(String.class))).thenThrow(new HibernateException("error test"));

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> commentaryService.deleteCommentary("test"));

        assertThat(exception.getMessage()).isEqualTo("error during commentary deletion : error test");
    }

}
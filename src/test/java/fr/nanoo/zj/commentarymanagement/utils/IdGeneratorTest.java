package fr.nanoo.zj.commentarymanagement.utils;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
class IdGeneratorTest {

    @Test
    void whenGenerateExternalId_ThenReturnIdWithGoodPattern(){
        assertThat(IdGenerator.generateExternalId())
                .matches("(\\w{8})([-])(\\w{4})([-])(\\w{4})([-])(\\w{4})([-])(\\w{12})");
    }

}